<?php
class meliOrder extends meli
{
  public function __construct()
  {
    parent::__construct();
  }

  public function meliGetOrder($order_id)
  {
    $response = $this->RestClientApi->resourceGet("/orders/$order_id",$this->access_token);
    $response = json_decode($response[0]);
    return $response;
  }

  public function meliGetRecentOrders()
  {
    $resource = "/orders/search/recent?seller=".USER_ID;

    $response = $this->RestClientApi->resourceGet($resource,$this->access_token);
    $response = json_decode($response[0]);

    return $response;
  }

  public function meliGetOrders()
  {
    $resource = "/orders/search?seller=".USER_ID."&order.status=paid";

    $response = $this->RestClientApi->resourceGet($resource,$this->access_token);
    $response = json_decode($response[0]);

    return $response;

  }

  public function meliGetShippingInformation($shippingId)
  {
    $resource = "/shipments/$shippingId";

    $response = $this->RestClientApi->resourceGet($resource,$this->access_token);
    $response = json_decode($response[0]);

    return $response;
  }

  public function meliGetOrderIds()
  {
    $recentOrders = $this->meliGetRecentOrders();

    $anotherPackId;
    if(empty($recentOrders->results)) return false;
    foreach ($recentOrders->results as $key => $value) {
      if(count($recentOrders->results) == 1) {
        // $shippingInfo = $this->meliGetShippingInformation($value->shipping->id);
        if($value->shipping->status != 'delivered' || $value->shipping->status != 'shipped') {
          $ordersId[] = $value->id;
        }
      } else {
        // var_dump(empty($value->pack_id));
        // exit;
        // $shippingInfo = $this->meliGetShippingInformation($value->shipping->id);

        if(isset($value->shipping->status) && $value->shipping->status == "delivered" || $value->shipping->status == "shipped") {

        } else {
          $packId = $value->pack_id;
          if(empty($packId)) $ordersId[] = $value->id;
          else $ordersId['pack_id'.$packId][] = $value->id;
        }
      }
    }

    if(!isset($ordersId)) return false;

    // var_dump($ordersId);
    // exit;
    return $ordersId;
  }

  public function meliGetOrderLabel($shipment_ids, $order_id)
  {
    $filename = "etiquetas/$order_id.pdf";
    $curl_url =  "https://api.mercadolibre.com/shipment_labels?shipment_ids=$shipment_ids&response_type=pdf&access_token=$this->access_token";
    $out = fopen($filename,"w+");
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_FILE, $out);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_URL, $curl_url);
    curl_exec($ch);
    curl_close($ch);

    return $filename;
  }

  public function meliGetQuestions()
  {
    $resource = "/messages/unread";

    $response = $this->RestClientApi->resourceGet($resource,$this->access_token);
    $response = json_decode($response[0]);

    return $response;
  }
  public function meliGetQuestionsId()
  {
    $body = $this->meliGetQuestions();
    // var_dump($body); exit; //DEBUG

    if(isset($body->results)) $questionsNotReaded = $body->results;
    else return false;

    if(count($questionsNotReaded) < 1) return false;
      // var_dump($questionsNotReaded); exit; //DEBUG
    foreach ($questionsNotReaded as $key => $value) $quesionsId[] = $value->order_id;

    return $quesionsId;
  }

  // RECEBER MENSAGENS POR ID DA ORDER
  public function meliGetQuestionsByOrderId($resource)
  {
    $resource = "messages/".$resource;

    $response = $this->RestClientApi->resourceGet($resource,$this->access_token);
    $response = json_decode($response[0]);

    return $response;
  }

  //RECEBER POR ID DA MENSAGEM
  public function meliGetQuestionsByMessageId($messageId)
  {
    $resource = "messages/$messageId";

    $response = $this->RestClientApi->resourceGet($resource,$this->access_token);
    $response = json_decode($response[0]);

    return $response;
  }

  //MARCAR MENSAGENS COMO LIDAS  *$messages_id se <1 separar por ,
  public function meliUpdateQuestionStatus($messageId)
  {
    $resource = "messages/mark_as_read/$messageId";

    $response = $this->RestClientApi->resourcePut($resource,$this->access_token);
    $response = json_decode($response[0]);

    return $response;
  }

  public function meliQuestionAnswer($buyerId, $orderId, $productName, $answer)
  {
    $body = [
      "from" => [
        "user_id" => USER_ID
      ],
      "to" => [
        [
          "user_id" => $buyerId,
          "resource" => "orders",
          "resource_id" => $orderId,
          "site_id" => "MLB"
        ]
      ],
      "subject" => $productName,
      "text" => [
        "plain" => $answer
      ]
    ];

    $resource = "/messages";

    $response = $this->RestClientApi->resourcePost($resource,$this->access_token,$body);
    $response = json_decode($response[0]);

    return $response;
  }

  public function meliSendNfe($pack_id,$uri)
  {
    $body = [
      'file' => 'fiscal_document=@'.$uri
    ];
    // var_dump($params,$body);
    // exit;
    $resource = "packs/$pack_id/fiscal_documents";

    $response = $this->RestClientApi->resourcePost($resource,$this->access_token,$body);
    $response = json_decode($response[0]);

    return $response;
  }
}
?>
