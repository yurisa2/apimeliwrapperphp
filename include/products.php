<?php
class meliProduct extends meli
{
  public function __construct()
  {
    parent::__construct();
  }

  public function meliGetProductsSkuByWcId($wcid)
  {
    foreach ($this->meliGetProducts() as $key => $value) {
      $meliProduct = $this->meliGetProduct($value);
      foreach($meliProduct->attributes as $key => $value) {
        if($value->name == 'Modelo' && $value->value_name == $wcid) return $meliProduct->permalink;
      }
    }

    return false;
  }
  public function meliGetProducts()
  {
    $limit  = 100;
    $offset = 0;
    $access_token = $this->access_token; // string |

    $resource = 'users/'.USER_ID.'/items/search?limit='.$limit.'&offset='.$offset; // string | for example: items
    $response = $this->RestClientApi->resourceGet($resource,$access_token);
    $response = json_decode($response[0]);
    if(is_null($response->results) || !is_array($response->results)) return array('httpCode' => 200 , 'message' => 'Não foi encontrado produtos no Mercado Livre');

    // if($response['httpCode'] > 399) return array('httpCode' => $response['httpCode'] , 'message' => $response['body']->message);
    if($response->paging->total < $limit || (count($response->results) < 100 && is_array($response->results)) ) return $response->results;

    if(($response->paging->total - $limit)/100 < 1) $i = 1;
    else $i = floor(($response->paging->total - $limit)/100);

    $allProductIds = [];
    $scroll_id = '';
    for ($incr=0; $incr < $i; $incr++) {
      if($incr == 9) {
        $resource2 = 'users/'.USER_ID.'/items/search?search_type=scan'; // string | for example: items

        $getscrollid = $this->RestClientApi->resourceGet($resource2,$access_token);
        $getscrollid = json_decode($getscrollid[0]);

        if( isset($getscrollid->scroll_id) ) $scroll_id = $getscrollid->scroll_id;
      }
      $offset = $offset+$limit;
      $resource2 = 'users/'.USER_ID.'/items/search?limit='.$limit.'&offset='.$offset; // string | for example: items

      if($incr >= 9) {
        $resource2['search_type'] = 'scan';
        $resource2['scroll_id'] = $scroll_id;
      }

      $productsId2 = $this->RestClientApi->resourceGet($resource2,$access_token);
      $productsId2 = json_decode($productsId2[0]);
      if(is_array($productsId2->results)) $allProductIds = array_merge($allProductIds,$productsId2->results);
    }

    if(is_array($response->results) && is_array($allProductIds)) {
      $allProductsId = array_unique(array_merge($response->results,$allProductIds));
      return $allProductsId;
    }

    return false;
  }

  public function meliGetProductsSku()
  {
    $meliProducts = $this->meliGetProducts();
    foreach ($meliProducts as $key => $value) {
      $productSku[] = $this->meliGetProductSku($value);
    }

    return $productSku;
  }

  public function meliGetProduct($productId)
  {
    if(substr($productId,0,3) != 'MLB') $productId = 'MLB'.$productId;

    $return = $this->RestClientApi->resourceGet('items/'.$productId,$this->access_token);
    $return = json_decode($return[0]);
    return $return;
  }

  public function meliGetProductSku($productId)
  {
    if(substr($productId,0,3) != 'MLB') $productId = 'MLB'.$productId;

    $resource = 'items/'.$productId.'?attributes=attributes&include_internal_attributes=true';

    $return = $this->RestClientApi->resourceGet($resource,$this->access_token);
    $return = json_decode($return[0]);
    foreach ($return->attributes as $key => $value) {
      if($value->name == "Modelo") return $value->value_name;
    }
    return false;
  }

  public function meliUpdateProduct($productId,$productData)
  {
    if(substr($productId,0,3) != 'MLB') $productId = 'MLB'.$productId;
    if(isset($productData['title'])) $body['title'] = $productData['title'];
    if(isset($productData['price'])) $body['price'] = $productData['price'];
    if(isset($productData['available_quantity'])) $body['available_quantity'] = $productData['available_quantity'];
    if(isset($productData['brand'])) $body['attributes'][] = array('name' => "Marca",'value_name' => $productData['brand']);
    if(isset($productData['sku'])) $body['attributes'][] = array('id' => "MODEL",'value_name' => $productData['sku']);

    if(!isset($body)) return false;

    $return = $this->RestClientApi->resourcePut("/items/$productId",$this->access_token,$body);
    $return = json_decode($return[0]);

    return $return;
  }


  public function meliGetProductDescription($productId)
  {
    $return = $this->RestClientApi->resourceGet("/items/$productId/description",$this->access_token);
    $return = json_decode($return[0]);
    return $return;
  }

  public function meliUpdateProductDescription($productId,$productDescription)
  {
    if(substr($productId,0,3) != 'MLB') $productId = 'MLB'.$productId;

    $body = array
    (
      'plain_text' => $productDescription
    );

    $return = $this->RestClientApi->resourcePut("/items/$productId/description",$this->access_token,$body);
    $return = json_decode($return[0]);

    return $return;
  }

  public function meliUpdateProductImages($productId, $imagesSrc)
  {
    if(substr($productId,0,3) != 'MLB') $productId = 'MLB'.$productId;
    foreach ($imagesSrc as $key => $value) {
      $srcUrl['pictures'][] = ['source' => $value];
    }
    $body = $srcUrl;

    $return = $this->RestClientApi->resourcePut("/items/$productId",$this->access_token,$body);
    $return = json_decode($return[0]);

    return $return;
  }

  public function meliGetProductCategory($productTitle)
  {
    $resource = "/sites/MLB/domain_discovery/search?q=".$productTitle;

    $return = $this->RestClientApi->resourceGet($resource,$this->access_token);
    $return = json_decode($return[0]);

    return $return;
  }

  public function meliGetProductCategoryMandatoryAttributes($productCategorie)
  {
    $return = $this->RestClientApi->resourceGet("/categories/$productCategorie/attributes",$this->access_token);
    $return = json_decode($return[0]);

    return $return;
  }


  public function meliCreateProduct($productData)
  {
    if(!empty($productData['title'])) $body['title'] = $productData['title'];
    if(!empty($productData['categoryId'])) $body['category_id'] = $productData['categoryId'];
    if(!empty($productData['price'])) $body['price'] = $productData['price'];
    $body['currency_id'] = "BRL";
    if(!empty($productData['qty'])) $body['available_quantity'] = $productData['qty'];
    $body['buying_mode'] = "buy_it_now";
    $body['listing_type_id'] = "gold_special";
    $body['condition'] = "new";
    if(!empty($productData['description'])) $body['description']['plain_text'] = $productData['description'];
    $body['warranty'] = "Garantia do vendedor => 90 dias";
    if(!empty($productData['images'])) $body['pictures'] = [ $productData['images'] ];
    if(!empty($productData['brand'])) $body['attributes'][] = [ "id" => "BRAND","name" => "Marca","value_name" => $productData['brand'] ];
    if(!empty($productData['sku'])) $body['attributes'][] = [ "id" => "MODEL","name" => "Modelo","value_name" => $productData['sku'] ];

    $return = $this->RestClientApi->resourcePost('items', $this->access_token, $body);
    $return = json_decode($return[0]);

    return $return;
  }

  public function meliGetQuestions()
  {
    $resource = '/questions/search?seller_id='.USER_ID.'&status=UNANSWERED';

    $return = $this->RestClientApi->resourceGet($resource,$this->access_token);
    $return = json_decode($return[0]);

    return $return;
  }

  public function meliGetQuestionIds()
  {
    $questions = $this->meliGetQuestions();

    if(count($questions->questions) < 1) return false;

    foreach ($questions->questions as $key => $value) {
      $QuestionsId[] = $value->item_id;
    }

    return array_unique($QuestionsId);
  }

  public function meliGetQuestion($questionId)
  {
    $return = $this->RestClientApi->resourceGet("/questions/$questionId",$this->access_token);
    $return = json_decode($return[0]);

    return $return;
  }

  public function meliAnswerQuestion($questionId, $answer)
  {
    $body = array('question_id' => $questionId,'text' => $answer);

    $return = $this->RestClientApi->resourcePost("/answers", $this->access_token, $body);
    $return = json_decode($return[0]);

    return $return;
  }
}

 ?>
