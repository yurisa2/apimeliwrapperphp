<?php

if(!isset($prefix_meli)) $prefix_meli = '';

include_once __DIR__ .$prefix_meli. '/meli-sdk/vendor/autoload.php';


// require_once $prefix_meli.'include/ml/php-sdk/Meli/meli.php';
// require_once $prefix_meli.'include/ml/php-sdk/configApp.php';

require_once $prefix_meli.'include/config.php';
require_once $prefix_meli.'include/defines.php';

require_once $prefix_meli.'include/meli.php';
require_once $prefix_meli.'include/products.php';
require_once $prefix_meli.'include/orders.php';
?>
