<?php
class meli
{

  /**
  * Constructor method. Set all constants to connect in Meli
  *
  * @param string APP_ID
  * @param string SECRET_KEY
  */
  public function __construct()
  {
    $this->config = new Meli\Configuration();
    $servers = $this->config->getHostSettings();

    // Use the correct auth URL
    $this->config->setHost($servers[2]["url"]);

    $this->OAuth20Api    = new Meli\Api\OAuth20Api( new GuzzleHttp\Client() );
    $this->RestClientApi = new Meli\Api\RestClientApi( new GuzzleHttp\Client( ) );

    $this->access_token = $this->meliRefreshToken();
  }

  /**
  * Executes a Request to refresh the access_token and refresh_token
  * NOTE: only be refreshed if the tokens expires.
  * NOTE: if access_token or refresh_token is null, the refresh don't work out
  *
  *
  */
  public function meliRefreshToken()
  {
    $grant_type = 'refresh_token'; // string |
    $client_id = APP_ID; // string |
    $client_secret = SECRET_KEY; // string |
    $redirect_uri = ''; // string |
    $code = ''; // string |
    $token = $this->meliGetToken(); // string |

    if( empty($token) || $token['expires_in'] <= time() ) {
      $refresh_token = $this->meliGetRefreshToken(); // string |
      try {
        $result = $this->OAuth20Api->getToken($grant_type, $client_id, $client_secret, $redirect_uri, $code, $refresh_token);
        $result = json_decode($result[0],true);
        $token = [
          'access_token' => $result['access_token'],
          'expires_in' => $result['expires_in'] + time(),
          'refresh_token' => $result['refresh_token'],
        ];
        $this->meliSetToken($token);

        return $token['access_token'];
      } catch (Exception $e) {
          echo 'Exception when calling OAuth20Api->getToken: ', $e->getMessage(), PHP_EOL;
      }
    }

    return $token['access_token'];
  }

  public function meliGetToken()
  {
    if ( !file_exists( 'include/files/token' ) ) file_put_contents( 'include/files/token', json_encode( '' ) );
    return json_decode( file_get_contents( 'include/files/token' ), true );
  }

  public function meliSetToken( array $token )
  {
    file_put_contents( 'include/files/token', json_encode( $token ) );
  }

  public function meliGetRefreshToken()
  {
    if ( !file_exists( 'include/files/token' ) ) return false;
    $token = $this->meliGetToken();

    if ( empty( $token ) ) return false;
    else return $token['refresh_token'];
  }
}
?>
